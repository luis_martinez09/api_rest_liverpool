from sqlalchemy import Column, Integer, String, DateTime, Float, ForeignKey
from sqlalchemy.orm import relationship
from src.database.database import Base, engine
from datetime import datetime


class Order(Base):
    __tablename__ = "orders"

    id = Column(Integer, primary_key=True, index=True)
    creation_date = Column(DateTime, default=datetime.now)
    cancellation_date = Column(DateTime, nullable=True)
    customer_name = Column(String, nullable=False)

    # Relación uno a muchos con la tabla OrderItem
    order_items = relationship("OrderItem", back_populates="order")


class OrderItem(Base):
    __tablename__ = "order_items"

    id = Column(Integer, primary_key=True, index=True)
    article_name = Column(String, nullable=False)
    price = Column(Float, nullable=False)
    quantity = Column(Integer, nullable=False)

    # Clave foránea para relacionar con la tabla Order
    order_id = Column(Integer, ForeignKey("orders.id"))
    order = relationship("Order", back_populates="order_items")


# Crear las tablas en la base de datos
Base.metadata.create_all(bind=engine)
