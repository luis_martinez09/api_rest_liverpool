from typing import List
from src.schemas.schema import OrderItemCreate, OrderResponse


def calculate_order_totals(items: List[OrderItemCreate]) -> dict:
    subtotal = sum(item.price * item.quantity for item in items)
    iva_rate = 0.16
    iva = subtotal * iva_rate
    total = subtotal + iva

    return {
        "subtotal": subtotal,
        "iva": iva,
        "total": total
    }


def create_order_response(order):
    items = [
        OrderItemCreate(
            article_name=item.article_name,
            price=item.price,
            quantity=item.quantity
        ) for item in order.order_items
    ]

    totals = calculate_order_totals(items)

    return OrderResponse(
        id=order.id,
        creation_date=order.creation_date.strftime("%d/%m/%Y"),
        cancellation_date=order.cancellation_date.strftime("%d/%m/%Y") if order.cancellation_date else None,
        customer_name=order.customer_name,
        items=items,
        **totals
    )
