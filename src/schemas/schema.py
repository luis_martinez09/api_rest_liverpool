from pydantic import BaseModel, validator, constr
from typing import List, Optional
from datetime import datetime


class OrderItemCreate(BaseModel):
    article_name: str
    price: float
    quantity: int

    @validator("article_name")
    def convert_to_upper(cls, value):
        return value.upper()

    @validator("price")
    def validate_price(cls, value):
        if value <= 0:
            raise ValueError("El precio debe ser mayor que cero")
        return value

    @validator("quantity")
    def validate_quantity(cls, value):
        if value <= 0:
            raise ValueError("La cantidad debe ser mayor que cero")
        return value


class OrderCreate(BaseModel):
    customer_name: constr(regex="^[a-zA-Z ]+$")
    items: List[OrderItemCreate]

    @validator("customer_name")
    def convert_to_upper(cls, value):
        return value.upper()


class OrderResponse(OrderCreate):
    id: int
    creation_date: str
    cancellation_date: Optional[str]
    customer_name: str
    items: List[OrderItemCreate]
    subtotal: float
    iva: float
    total: float


class OrderQueryParams(BaseModel):
    start_creation_date: Optional[datetime] = None
    end_creation_date: Optional[datetime] = None
    start_cancellation_date: Optional[datetime] = None
    end_cancellation_date: Optional[datetime] = None


class OrdersResponse(BaseModel):
    pass
