from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session, aliased
from pydantic import ValidationError
from src.schemas.schema import OrderCreate, OrderResponse
from src.database.database import get_db
from src.models.model import Order, OrderItem
from src.utils import create_order_response
from typing import List, Optional
from datetime import datetime


router = APIRouter(
    prefix='/api',
    tags=['Api'],

)


@router.post("/orders/", response_model=OrderResponse)
def create_order(order: OrderCreate, db: Session = Depends(get_db)):
    """Endpoint que permita la creación de órdenes de venta. Dicho endpoint debe
        solicitar la siguiente información
        - Nombre del cliente (comprador)
        - Artículo(s) comprado(s)
            - Nombre del artículo.
            - Precio.
            - Cantidad.
    """
    try:
        if order:
            db_order = Order(customer_name=order.customer_name)
            db.add(db_order)
            db.commit()
            db.refresh(db_order)

            for item in order.items:
                existing_item = db.query(OrderItem).filter(OrderItem.article_name == item.article_name).first()

                if existing_item:
                    price = existing_item.price
                else:
                    price = item.price

                db_item = OrderItem(
                    article_name=item.article_name,
                    price=price,
                    quantity=item.quantity,
                    order_id=db_order.id
                )
                db.add(db_item)

            db.commit()
            db.refresh(db_order)
            order_response = create_order_response(db_order)
            return order_response
    except ValidationError as e:
        return e.errors()


@router.get("/orders/{order_id}", response_model=OrderResponse)
def get_order(order_id: int, db: Session = Depends(get_db)):
    """Endpoint para consultar una orden de venta dado su ID.
    - ID
    """
    try:
        order = db.query(Order).filter(Order.id == order_id).first()
        if order:
            order_response = create_order_response(order)
            return order_response
        else:
            raise HTTPException(status_code=404, detail="Order not found")
    except ValidationError as e:
        return e.errors()


@router.get("/orders/", response_model=List[OrderResponse])
def get_orders(
        start_creation_date: Optional[str] = None,
        end_creation_date: Optional[str] = None,
        start_cancellation_date: Optional[str] = None,
        end_cancellation_date: Optional[str] = None,
        db: Session = Depends(get_db)
):
    order_alias = aliased(Order, name="order_alias")
    order_items_alias = aliased(OrderItem, name="order_items_alias")

    query = (
        db.query(Order)
        .join(order_alias, Order.id == order_alias.id)
        .join(order_items_alias, Order.id == order_items_alias.order_id)
    )

    if start_creation_date:
        start_creation_date = datetime.strptime(start_creation_date, "%Y-%m-%d")
        query = query.filter(Order.creation_date >= str(start_creation_date)[:10])
    if end_creation_date:
        end_creation_date = datetime.strptime(end_creation_date, "%Y-%m-%d")
        query = query.filter(Order.creation_date <= str(end_creation_date)[:10])

    if start_cancellation_date:
        start_cancellation_date = datetime.strptime(start_cancellation_date, "%Y-%m-%d")
        query = query.filter(Order.cancellation_date >= str(start_cancellation_date)[:10])
    if end_cancellation_date:
        end_cancellation_date = datetime.strptime(end_cancellation_date, "%Y-%m-%d")
        query = query.filter(Order.cancellation_date <= str(end_cancellation_date)[:10])

    order_responses = []
    for order in query:
        order_response = create_order_response(order)
        order_responses.append(order_response)

    return order_responses


@router.delete("/orders/{order_id}", response_model=OrderResponse)
def cancel_order(order_id: int, db: Session = Depends(get_db)):
    """Endpoint para cancelar una orden de venta dado su ID
        - ID
    """
    try:
        order = db.query(Order).filter(Order.id == order_id).first()
        if order:
            if not order.cancellation_date:
                order.cancellation_date = datetime.now()
                db.commit()
                db.refresh(order)

                order_response = create_order_response(order)
                return order_response
            else:
                raise HTTPException(status_code=400, detail="Order already canceled")
        else:
            raise HTTPException(status_code=404, detail="Order not found")
    except ValidationError as e:
        return e.errors()
