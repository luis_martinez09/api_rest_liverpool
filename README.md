# *Indicaciones
** Instalar el entorno (Docker, Environment) seguir los siguientes pasos:
### *Docker

La imagen que se crea es basada en python:3.8.3-slim-buster y para generarla utilizaremos los siguientes comandos

#### Construir la imagen
$ docker build -t luisapi:0.1 .

#### Desplegar el docker en el puerto 9000, este se podra consultar en http://localhost:9000/docs de nuestro navegador
$ docker run -ti --name luis-api -p 9000:8000 -d luisapi:0.1

### En caso de utilizar Environment seguir los siguientes pasos

#### Develop/Local Environment
- Para instalar dependencias, requerido `Python 3.8.x`, en terminal seguir los siguiente pasos para la instalación del entorno

```shell
$ python -m venv venv
$ source venv/bin/activate
(venv) $ pip install pip -U && pip install -r requirements.txt
````
- Ejecutar la aplicación
```shell
(venv) $ uvicorn main:app --reload
```

* En ambos casos docker o Environment automaticamente se crearan las tablas e insertara los datos de prueba

# Postman
#### Se anexa archivo con los endopoints para las prueabs en postman (API_ORDER.postman_collection.json)