from fastapi import FastAPI
from src.routers import router
from dotenv import load_dotenv
import os
load_dotenv()

app = FastAPI(
    debug=os.getenv("DEBUG"),
    title=os.getenv("TITLE"),
    description=os.getenv("DESCRIPTION"),
    version=os.getenv("PROJECT_VERSION"),
    contact={
        "name": os.getenv("CONTACT_NAME"),
        "email": os.getenv("CONTACT_EMAIL"),
    })
app.include_router(router.router)
