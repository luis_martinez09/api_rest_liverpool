FROM python:3.8.3-slim-buster

ENV WORK_DIR /var/www/api
# ENV WORK_DIR /opt/api

WORKDIR $WORK_DIR

COPY . .

RUN pip install -U pip; \
    pip install --no-cache-dir --upgrade -r requirements.txt

EXPOSE 8080/tcp

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
